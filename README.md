# Rock, Paper, Scissors

The Program
-------

This repo provides the code to play Rock, Paper, Scissors. It can be played as either a single player against the computer, or two players against each other. For the rules, see the following link: https://en.wikipedia.org/wiki/Rock-paper-scissors

Instructions for use
----

* Fork the repo and clone it into your own local files.
* From the command line change into the project directory and run bundle to make sure that the necessary gems are installed.
* Type 'ruby app.rb' into the command line while in the project root directory.
* Open your internet browser, and visit 'http://localhost:4567'
* You should now see the homepage of the game - follow the instructions on screen to play.
